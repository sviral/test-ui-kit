import React from 'react';
import ReactDOM from 'react-dom';
import {Button} from '../../lib/button';

class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello world</h1>
        <Button>Hello world</Button>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
